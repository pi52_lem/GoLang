<!DOCTYPE HTML>
<html>
<head>
	<title>{{ .Title }}</title>
	<style>
    body{
		background-color:#f5f5f5;  
		text-align:center;	 
    }
	h1{
		text-align:center;   
    }
    .content{
		margin:0 auto;
		width:60%;
		min-width:200px;
		min-width:200px;
    }
	.info{
		border-top:3px solid black;
	}
	.mainInfo{
		display:block;
	}
	footer{
		text-align:left;
	}
	
	</style>
</head>
<body>
<div class="content">
	<h1>{{ .Header }}</h1>
	<span class="mainInfo">{{ .Body }}</span>
<div class="links">
			<a href="/">Головна</a>
			<a href="/About">Про сайт</a>
			<a href="/Contacts">Контактна інформація</a>
			<a href="/Info">Інформація</a>
			<a href="/anotherPage">Ще одна сторінка</a>
		</div>
	<footer>{{ .Footer }}</footer>
</div>
</body>
</html>