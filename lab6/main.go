package main

import (
	"html/template"
	"net/http"
)

type Variables struct {
	Title  string
	Header string
	Body   string
	Footer string
}

const IndexPage = "main.tpl"
const AboutPage = "about.tpl"
const ContactsPage = "contacts.tpl"
const InfoPage = "info.tpl"
const anotherPage = "anotherPage.tpl"

var TPLpath = ""

const (
	anError = `<p class="error">%s</p>`
)

type Solution string

var HttpSolution1 Solution = "Задание1"

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := Index()
	switch r.URL.Path {
	case "/About":
		vars = About()
	case "/Contacts":
		vars = Contacts()
	case "/Info":
		vars = Info()
	case "/anotherPage":
		vars = anotherP()
	}
	tmpl, err := template.ParseFiles(TPLpath)
	templates := template.Must(tmpl, err)
	templates.ExecuteTemplate(w, TPLpath, vars)
}

func Index() Variables {
	vars := Variables{}
	vars.Title = "Main"
	vars.Header = "Лабораторна 6"
	vars.Body = "Веб-сайт"
	vars.Footer = "@2017"

	TPLpath = IndexPage
	return vars
}

func About() Variables {
	vars := Variables{}
	vars.Title = "About"
	vars.Header = "Про сайт"
	vars.Body = "Даний веб-сайт розроблений для лабораторної роботи №6"
	vars.Footer = "@2017"

	TPLpath = AboutPage
	return vars
}

func Contacts() Variables {
	vars := Variables{}
	vars.Title = "Contacts"
	vars.Header = "Інформація"
	vars.Body = "Мої контакти: +380677651293."
	vars.Footer = "@2017"

	TPLpath = ContactsPage
	return vars
}

func Info() Variables {
	vars := Variables{}
	vars.Title = "Info"
	vars.Header = "Інформація"
	vars.Body = "Лісовий Євгеній, Група ПІ-52, курс 3."
	vars.Footer = "@2017"

	TPLpath = InfoPage
	return vars
}

func anotherP() Variables {
	vars := Variables{}
	vars.Title = "anotherPage"
	vars.Header = "Ще одна сторінка"
	vars.Body = "Ще одна сторінка"
	vars.Footer = "@2017"

	TPLpath = anotherPage
	return vars
}

func main() {
	// Запускаем локальный сервер
	http.ListenAndServe("localhost:2001", HttpSolution1)
}
