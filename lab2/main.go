// Laba2 project main.go
package main

import (
	"fmt"

	mypack "./mypack"
)

func main() {
	fmt.Println(mypack.Min(6, 10, 3))
	fmt.Println(mypack.MyAvarage(13, 7, 6))
	fmt.Println("a * x + b = 0")
	var a float64
	var b float64
	fmt.Println("Enter a")
	fmt.Scanf("%f \n", &a)
	fmt.Println("Enter b")
	fmt.Scanf("%f \n", &b)
	fmt.Println("x = ", mypack.LinRivn(a, b))
}

func Min(a float64, b float64, c float64) float64 {
	min := a
	if min > b {
		min = b
	}
	if min > c {
		min = c
	}
	return min
}

func myAvarage(a float64, b float64, c float64) float64 {
	return (a + b + c) / 3
}

func linRivn(a float64, b float64) float64 {
	return (-1 * b) / a
}
