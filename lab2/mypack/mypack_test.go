package mypack

import "testing"

func TestMin(t *testing.T) {
	x := Min(1, 2, -3)
	res := float64(-3)
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}
func TestMyAvarage(t *testing.T) {
	x := MyAvarage(6, 3, -3)
	res := float64(2)
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}
func TestLinRivn(t *testing.T) {
	x := LinRivn(3, 6)
	res := float64(-2)
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}
