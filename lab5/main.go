package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
)

const (
	pageHeader = `<!DOCTYPE HTML>
 <html>
 <head>
 <title>Лаба 5</title>
 <style>
 .error{
 color:#FF0000;
 }
 </style>
 </head>`
	pageBody = `<body>`
	form     = `<form action="/" method="POST">
 <label for="numbers">Введите число:</label><br />
 <input type="text" name="numbers" size="20" value="5"><br />
 <input type="submit" value="Выполнить">
 </form>`
	pageFooter = `</body></html>`
	table      = `<table border="1">
 <tr><th colspan="2">Результат</th></tr>
 <tr><td>Строка 1</td><td>%s</td></tr>
 <tr><td>Строка 2</td><td>%f</td></tr>
 </table>`
	anError     = `<p class="error">%s</p>`
	charset     = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	formStudent = `<form action="/task5Post" method="post">
		<label form="name">Ім'я</label><input type="text" name="name" id="name"/><br>
		<label form="firstname">Прізвище</label><input type="text" name="firstname" id="firstname"/><br>
		<label form="group">Група</label><input type="text" name="group" id="group" /><br>
		<input type="submit" value="OK" />
	</form>`
	arrayNumber = 17
	hTask1      = "<h1>Завдання 1</h1>"
	textTask1   = `<p>Написати програму, яка заповнює одновимірний масив цілими числами відповідно до варіанта. <br>Результат вивести на екран монітора.`
	hTask2      = "<h1>Завдання 2</h1>"
	textTask2   = `<p>Написати програму, яка генерує зріз з випадкових символів відповідно до варіанта. Результат вивести на екран монітора.</p1>`
	hTask3      = "<h1>Завдання 3</h1>"
	textTask3   = `<p>Написати програму, яка заповнює одновимірний масив дійсними числами відповідно до варіанта. Результат вивести на екран монітора.</p1>`
	hTask4      = "<h1>Завдання 4</h1>"
	textTask4   = `<p>Написати програму, яка заповнює одновимірний зріз дійсними числами відповідно до варіанта (використати данні завдання 3). Виконати розрахунки у відповідності з варіантом. Результат вивести на екран монітора.</p1>`
)

type Solution string

var HttpSolution1 Solution = ""

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, pageHeader, pageBody, form) // Формируем страницу в браузере
	if r.Method == "POST" {                   // Обрабатываем входные данные
		err := r.ParseForm() // Парсим форму
		post := r.PostForm
		if err != nil {
			fmt.Fprintf(w, anError, err)
			return
		}
		pnumbers := post.Get("numbers")
		numbers, _ := strconv.ParseInt(pnumbers, 10, 32)
		fmt.Fprintf(w, "%v", numbers) // Выводим сообщение в браузер
		fmt.Fprintf(w, "\n\t<br /><br />")
		fmt.Fprintf(w, table, "Текст", 5.6)
	}
	fmt.Fprint(w, "\n", pageFooter)
}

type Student struct {
	Name    string
	Surname string
	Group   string
}

func task1(w http.ResponseWriter, r *http.Request) {
	var array [arrayNumber]int
	var output string = "<ol>"
	for i := 0; i < len(array); i++ {
		randEl := rand.Intn(10) - 15
		if randEl == -15 {
			randEl = -14
		}
		array[i] = randEl
		output += strings.Join([]string{"<li>", strconv.Itoa(array[i]), "</li>"}, " ")
	}
	output += "</ol>"
	fmt.Fprint(w, pageHeader, pageBody, hTask1, textTask1, `Кількість елементів`, arrayNumber, ". Діапазон (-15, -5]", `</p1>`)
	fmt.Fprint(w, output)
	fmt.Fprint(w, "\n", pageFooter)
}
func task2(w http.ResponseWriter, r *http.Request) {
	var array []string
	var output string = ""
	for i := 0; i < 100; i++ {
		array = append(array, string(charset[rand.Intn(26)]))
		output += strings.Join([]string{" ", array[i]}, " ")
	}
	fmt.Fprint(w, pageHeader, pageBody, hTask2, textTask2)
	fmt.Fprint(w, output)
	fmt.Fprint(w, "\n", pageFooter)
}
func task3(w http.ResponseWriter, r *http.Request) {
	var array [arrayNumber]float64
	var output string = "<ol>"
	for i := 0; i < len(array); i++ {
		randEl := rand.Float64()*(10.0) - 15
		if randEl == -15.0 {
			randEl = -14.99999999
		}
		array[i] = randEl
		output += strings.Join([]string{"<li>", strconv.FormatFloat(array[i], 'f', 6, 64), "</li>"}, " ")
	}
	output += "</ol>"
	fmt.Fprint(w, pageHeader, pageBody, hTask3, textTask3)
	fmt.Fprint(w, output)
	fmt.Fprint(w, "\n", pageFooter)
}

func task4(w http.ResponseWriter, r *http.Request) {
	_slice := make([]float64, arrayNumber)
	var sum float64 = 0.0
	var mul float64 = 1
	var output string = "<ol>"
	for i := 0; i < arrayNumber; i++ {
		randEl := rand.Float64()*(10.0) - 15.0
		if randEl == -15.0 {
			randEl = -14.99999999
		}
		_slice[i] = randEl
	}
	for i := 0; i < arrayNumber; i++ {
		output += strings.Join([]string{"<li>", strconv.FormatFloat(_slice[i], 'f', 6, 64), "</li>"}, " ")
		if _slice[i] < 0 {
			sum += _slice[i]
		}
		mul *= _slice[i]
	}
	output += "</ol>"
	output += "<p>Сума : " + strconv.FormatFloat(sum, 'f', 6, 64) + "</p>"
	output += "<p>Добуток : " + strconv.FormatFloat(mul, 'f', 6, 64) + "</p>"

	fmt.Fprint(w, pageHeader, pageBody, hTask4, textTask4)
	fmt.Fprint(w, output)
	fmt.Fprint(w, "\n", pageFooter)
}
func formStud(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, pageHeader, pageBody)
	fmt.Fprint(w, formStudent)
	fmt.Fprint(w, "\n", pageFooter)
}
func task5Post(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, pageHeader, pageBody)
	err := r.ParseForm() // Парсим форму
	post := r.PostForm
	if err != nil {
		fmt.Fprintf(w, anError, err)
		return
	}
	student := make(map[string]string)
	student["Ім'я"] = post.Get("name")
	student["Прізвище"] = post.Get("firstname")
	student["Група"] = post.Get("group")
	for key, value := range student {
		fmt.Fprint(w, "<p>", key, " : ", value, "</p>")
	}
	fmt.Fprint(w, "\n<a href='/task5'>Повернутися на форму</a>", pageFooter)
}

func main() {
	http.HandleFunc("/task1", task1)
	http.HandleFunc("/task2", task2)
	http.HandleFunc("/task3", task3)
	http.HandleFunc("/task4", task4)
	http.HandleFunc("/task5", formStud)
	http.HandleFunc("/task5Post", task5Post)
	http.ListenAndServe("localhost:2001", nil)

}
