package main

import (
	"strconv"

	"github.com/andlabs/ui"
)

func initGUI() {
	window := ui.NewWindow("Тур вихідного дня", 200, 150, false)

	tur := ui.NewLabel("Тур")

	days := ui.NewHorizontalBox()
	daysGLable := ui.NewLabel("Кількість днів:        ")
	daysGTxBox := ui.NewEntry()
	days.Append(daysGLable, false)
	days.Append(daysGTxBox, false)

	tikets := ui.NewHorizontalBox()
	tiketsGLable := ui.NewLabel("Кількість білетів:   ")
	tiketsGTxBox := ui.NewEntry()
	tikets.Append(tiketsGLable, false)
	tikets.Append(tiketsGTxBox, false)

	margin := ui.NewHorizontalBox()
	marginGLable := ui.NewLabel(" ")
	margin.Append(marginGLable, false)

	country := ui.NewCombobox()
	country.Append("Болгария")
	country.Append("Германия")
	country.Append("Польша")

	costs := ui.NewLabel("Вартість:0")

	//======
	seasonLabel := ui.NewLabel("Пора року")

	season := ui.NewCombobox()
	season.Append("Літо")
	season.Append("Зима")

	Guide := ui.NewCheckbox("Гід")
	lux := ui.NewCheckbox("Номер-люкс")

	calc := ui.NewButton("Розрахувати")

	boxMain := ui.NewHorizontalBox()
	box1 := ui.NewVerticalBox()
	box2 := ui.NewVerticalBox()

	box1.Append(tur, false)
	box1.Append(days, false)
	box1.Append(tikets, false)
	box1.Append(margin, false)
	box1.Append(country, false)
	box1.Append(costs, false)

	box2.Append(seasonLabel, false)
	box2.Append(season, false)
	box2.Append(Guide, false)
	box2.Append(lux, false)
	box2.Append(calc, false)

	boxMain.Append(box1, false)
	boxMain.Append(box2, false)
	window.SetChild(boxMain)

	calc.OnClicked(func(*ui.Button) {
		d, err1 := strconv.ParseFloat(daysGTxBox.Text(), 64)
		t, err2 := strconv.ParseFloat(tiketsGTxBox.Text(), 64)
		//
		err1 = err2
		err2 = err1
		//
		price := -1.0
		switch country.Selected() {
		case 0:
			if season.Selected() == 0 {
				price = 100
			} else {
				price = 150
			}
		case 1:
			if season.Selected() == 0 {
				price = 160
			} else {
				price = 200
			}
		case 2:
			if season.Selected() == 0 {
				price = 120
			} else {
				price = 180
			}
		}
		price *= d * t
		if Guide.Checked() {
			price += d * t * 50
		}
		if lux.Checked() {
			price += price * 0.2
		}

		costs.SetText("Вартість: $" + strconv.FormatFloat(price, 'f', 6, 64))
	})

	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})

	window.Show()
}

func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
