package main

import (
	"strconv"

	"github.com/andlabs/ui"
)

func initGUI() {
	window := ui.NewWindow("Завдання 1", 220, 150, false)

	sizesLabel := ui.NewLabel("Розміри вікна")

	width := ui.NewHorizontalBox()
	widthGLable := ui.NewLabel("Ширина:")
	widthGTxBox := ui.NewEntry()
	width.Append(widthGLable, false)
	width.Append(widthGTxBox, false)

	height := ui.NewHorizontalBox()
	heightGLable := ui.NewLabel("Висота:   ")
	heightGTxBox := ui.NewEntry()
	height.Append(heightGLable, false)
	height.Append(heightGTxBox, false)

	material := ui.NewCombobox()
	material.Append("Дерев'яний")
	material.Append("Металевий")
	material.Append("металопластиковий")

	costs := ui.NewLabel("Вартість:0")

	//======
	paket := ui.NewLabel("Склопакет")

	Kamers := ui.NewCombobox()
	Kamers.Append("Однокамерний")
	Kamers.Append("Двукамерний")

	pidv := ui.NewCheckbox("Підвіконня")

	calc := ui.NewButton("Розрахувати")

	boxMain := ui.NewHorizontalBox()
	box1 := ui.NewVerticalBox()
	box2 := ui.NewVerticalBox()

	box1.Append(sizesLabel, false)
	box1.Append(width, false)
	box1.Append(height, false)
	box1.Append(material, false)
	box1.Append(costs, false)

	box2.Append(paket, false)
	box2.Append(Kamers, false)
	box2.Append(pidv, false)
	box2.Append(calc, false)

	boxMain.Append(box1, false)
	boxMain.Append(box2, false)
	window.SetChild(boxMain)

	calc.OnClicked(func(*ui.Button) {
		w, err1 := strconv.ParseFloat(widthGTxBox.Text(), 64)
		h, err2 := strconv.ParseFloat(heightGTxBox.Text(), 64)
		Square := w * h
		//
		err1 = err2
		err2 = err1
		//
		pr := -1.0
		switch material.Selected() {
		case 0:
			if Kamers.Selected() == 0 {
				pr = 0.25 * Square
			} else {
				pr = 0.3 * Square
			}
		case 1:
			if Kamers.Selected() == 0 {
				pr = 0.05 * Square
			} else {
				pr = 0.1 * Square
			}
		case 2:
			if Kamers.Selected() == 0 {
				pr = 0.15 * Square
			} else {
				pr = 0.20 * Square
			}
		}

		if pidv.Checked() {
			pr += 35
		}

		costs.SetText("Вартість:" + strconv.FormatFloat(pr, 'f', 6, 64))
	})

	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})

	window.Show()
}

func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
