package main

import (
	"fmt"
	"math"
)

func main() {
	numberCount := 3000
	x := int64(1)
	var array [3000]int64
	for i := 0; i < numberCount; i++ {
		array[i] = Congur(&x)
	}
	arr := GetIm(array)
	for k := range arr {
		fmt.Println(k, " - ", arr[k])
	}
	spodiv := MathSpodiv(arr)
	fmt.Println("Математичне сподівання : ", spodiv)
	disp := Disp(arr, spodiv)
	avDisp := math.Sqrt(disp)
	fmt.Println("Дисперсія : ", disp)
	fmt.Println("Середньоквадратичне відхилення : ", avDisp)
}

func Congur(x0 *int64) int64 {
	m := int64(math.Pow(2, 31) - 1)
	a := int64(16807)
	c := int64(0)
	res := int64(0)
	*(x0) = ((a * *(x0)) + c) % m
	res = *(x0) % int64(200)
	return res
}

func GetIm(array [3000]int64) [3000]float64 {
	var arr [3000]float64
	for i := 0; i < len(array); i++ {
		wasChecked := true
		if arr[array[i]] == 0 {
			wasChecked = false
		}
		if !wasChecked {
			count := 0
			for j := 0; j < len(array); j++ {
				if array[j] == array[i] {
					count++
				}
			}
			arr[array[i]] = float64(count) / 3000
		}
	}
	return arr
}

func MathSpodiv(arr [3000]float64) float64 {
	var spodiv float64 = 0
	for k := range arr {
		spodiv = spodiv + arr[k]*float64(k)
	}
	return spodiv
}
func MathSpodivSquare(arr [3000]float64) float64 {
	var spodiv float64 = 0
	for k := range arr {
		spodiv = spodiv + arr[k]*float64(k)*float64(k)
	}
	return spodiv
}

func Disp(arr [3000]float64, mathSpodiv float64) float64 {
	var disp float64 = 0
	spodivSquare := MathSpodivSquare(arr)
	disp = spodivSquare - mathSpodiv*mathSpodiv
	return disp
}
