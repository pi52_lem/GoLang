package main

import (
	"fmt"
	"math"
)

func main() {
	numberCount := 3000
	x := int64(1)
	var array [3000]float64
	for i := 0; i < numberCount; i++ {
		array[i] = Congur(&x)
	}
	mMap := GetIm(array)
	for k := range mMap {
		fmt.Println(k, " - ", mMap[k])
	}
	spodiv := MathSpodiv(mMap)
	fmt.Println("Математичне сподівання : ", spodiv)
	disp := Disp(mMap, spodiv)
	fmt.Println("Дисперсія : ", disp)
	fmt.Println("Середньоквадратичне відхилення : ", math.Sqrt(disp))
}

func Congur(x0 *int64) float64 {
	m := int64(math.Pow(2, 31) - 1)
	a := int64(16807)
	c := int64(0)
	res := float64(0)
	for {
		*(x0) = ((a * *(x0)) + c) % m
		res = float64(*(x0)) / float64(m) * 1000
		if res <= 200 {
			break
		}
	}
	return toFixed(res, 2)
}

func IsInMap(mass map[float64]float64, key float64) bool {
	if mass[key] == 0 {
		return false
	}
	return true
}

func GetNumCountInArray(array [3000]float64, num float64) int64 {
	count := 0
	for i := 0; i < len(array); i++ {
		if array[i] == num {
			count++
		}
	}
	return int64(count)
}

func GetIm(array [3000]float64) map[float64]float64 {
	myMap := make(map[float64]float64)
	for i := 0; i < len(array); i++ {
		wasChecked := true
		if myMap[array[i]] == 0 {
			wasChecked = false
		}
		if !wasChecked {
			count := 0
			for j := 0; j < len(array); j++ {
				if array[j] == array[i] {
					count++
				}
			}
			myMap[array[i]] = float64(count) / 3000.0
		}
	}
	return myMap
}

func MathSpodiv(myMap map[float64]float64) float64 {
	var spodiv float64 = 0
	for k := range myMap {
		spodiv = spodiv + myMap[k]*float64(k)
	}
	return spodiv
}
func MathSpodivSquare(myMap map[float64]float64) float64 {
	var spodiv float64 = 0
	for k := range myMap {
		spodiv = spodiv + myMap[k]*float64(k)*float64(k)
	}
	return spodiv
}

func Disp(myMap map[float64]float64, mathSpodiv float64) float64 {
	var disp float64 = 0
	spodivSquare := MathSpodivSquare(myMap)
	disp = spodivSquare - mathSpodiv*mathSpodiv
	return disp
}

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}
