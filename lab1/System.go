package main

import "fmt"

func main() {
	var x1, x2, a1, a2, b1, b2, c1, c2 float32
	/*
	   a1*x1 + a2*x2 = c1
	   b1*x1 + b2*x2 = c2
	*/
	fmt.Println("a1 =")
	fmt.Scanf("%f \n", &a1)
	fmt.Println("a2 =")
	fmt.Scanf("%f \n", &a2)
	fmt.Println("b1 =")
	fmt.Scanf("%f \n", &b1)
	fmt.Println("b2 =")
	fmt.Scanf("%f \n", &b2)
	fmt.Println("c1 =")
	fmt.Scanf("%f \n", &c1)
	fmt.Println("c2 =")
	fmt.Scanf("%f \n", &c2)

	fmt.Printf("%f*x1 + (%f) = %f\n", a1, a2, c1)
	fmt.Printf("%f*x1 + (%f) = %f\n", b1, b2, c2)

	// способом додавання
	a := a1 + b1
	b := b2 + a2
	c := c1 + c2

	// x1 = (c - b*x2)/a
	// a1 * (c - b*x2)/a + a2 * x2 = c1
	// x2 = (c1*a - a1*c)/ (a*a2 - a1*b)
	tmp := (c1*a - a1*c)
	tmp2 := (a*a2 - a1*b)
	x2 = tmp / tmp2
	x1 = (c - b*x2) / a
	fmt.Printf("x1 = %f; x2 = %f", x1, x2)
}
