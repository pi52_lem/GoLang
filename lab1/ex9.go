package main

import "fmt"

func main() {
	var first, second bool
	var third bool = true
	fourth := !third
	var fifth = true

	fmt.Println("first  = ", first)       // false
	fmt.Println("second = ", second)      // false
	fmt.Println("third  = ", third)       // true
	fmt.Println("fourth = ", fourth)      // false
	fmt.Println("fifth  = ", fifth, "\n") // true

	fmt.Println("!true  = ", !true)        // false
	fmt.Println("!false = ", !false, "\n") // true

	fmt.Println("true && true   = ", true && true)         // true
	fmt.Println("true && false  = ", true && false)        // false
	fmt.Println("false && false = ", false && false, "\n") // false

	fmt.Println("true || true   = ", true || true)         // true
	fmt.Println("true || false  = ", true || false)        // true
	fmt.Println("false || false = ", false || false, "\n") // false

	fmt.Println("2 < 3  = ", 2 < 3)        // true
	fmt.Println("2 > 3  = ", 2 > 3)        // false
	fmt.Println("3 < 3  = ", 3 < 3)        // false
	fmt.Println("3 <= 3 = ", 3 <= 3)       // true
	fmt.Println("3 > 3  = ", 3 > 3)        // false
	fmt.Println("3 >= 3 = ", 3 >= 3)       // true
	fmt.Println("2 == 3 = ", 2 == 3)       // false
	fmt.Println("3 == 3 = ", 3 == 3)       // true
	fmt.Println("2 != 3 = ", 2 != 3)       // true
	fmt.Println("3 != 3 = ", 3 != 3, "\n") // false

	//Задание.
	//1. Пояснить результаты операций
	/*
		2 < 3  => true  - перевірка чи значення зліва (2) меньше значення справа (3) - 2 дійсно меньше 3
		2 > 3  => false - перевірка чи значення зліва (2) більше значення справа (3) - 2 меньше 3
		3 < 3  => false - перевірка чи значення зліва (3) меньше значення справа (3) - 3 не меньше 3
		3 <= 3 => true  - перевірка чи значення зліва (3) меньше або рівне значенню справа (3) - 3 рівне 3
		3 > 3  => false - перевірка чи значення зліва (3) більше значення справа (3) - 3 не більше 3
		3 >= 3 => true  - перевірка чи значення зліва (3) більше або рівне значенню справа (3) - 3 рівне 3
		2 == 3 => false - перевірка чи значення зліва (2) рівне значенню справа (3) - 2 не рівне 3
		3 == 3 => true  - перевірка чи значення зліва (3) рівне значенню справа (3) - 3 рівне 3
		2 != 3 => true  - перевірка чи значення зліва (2) не рівне значенню справа (3) - 2 не рівне 3
		3 != 3 => false - перевірка чи значення зліва (3) не рівне значенню справа (3) - 3 рівне 3
	*/
}
