package main

import "fmt"
import "unsafe"

func main() {
	fmt.Println("Синонимы целых типов\n")

	fmt.Println("byte    - int8")
	fmt.Println("rune    - int32")
	fmt.Println("int     - int32, или int64, в зависимости от ОС")
	fmt.Println("uint    - uint32, или uint64, в зависимости от ОС")

	//Задание.
	//1. Определить разрядность ОС
	var r rune = 10
	var i int = 10
	if unsafe.Sizeof(r) == unsafe.Sizeof(i) {
		fmt.Println("разрядность ОС - 32\n")
	} else {
		fmt.Printf("разрядность ОС - 64\n")
	}
}
