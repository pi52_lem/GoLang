package main

import "fmt"

func main() {
	var chartype int8 = 'R'

	fmt.Printf("Code '%c' - %d\n", chartype, chartype)

	//Задание.
	//1. Вывести украинскую букву 'Ї'
	//2. Пояснить назначение типа "rune"
	var chartype2 rune = 'Ї'
	fmt.Printf("Code '%c' - %d\n", chartype2, chartype2)
	fmt.Printf("Переменная типа 'rune' содержит число, еквивалентное числу буквы в юникоде.\nТоесть 'var c rune = 97' тоже самое что 'var c rune = \"a\"'")
}
