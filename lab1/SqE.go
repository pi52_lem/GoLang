package main

import "fmt"
import "math"

func main() {
	var a = 1
	var b = 0
	var c = -1

	dis := (float64)(b*b - 4*a*c)
	var tmp float64
	if dis < 0 {
		fmt.Println("дискриминант меньше 0, поэтому данное уравнение не имеет корней")
	} else {
		if dis == 0 {
			fmt.Println("один корень:x = ")
			tmp = (float64)(-b / (2 * a))
			fmt.Println(tmp)
		} else {
			if a == 0 {
				fmt.Println("один корень: x = ")
				tmp = (float64)(-c / b)
				fmt.Println(tmp)
			} else {
				fmt.Println("два различных корня: x1 = ")
				sqrt := (float64)(math.Sqrt(dis))
				twoA := (float64)(2 * a)
				tmp2 := (float64)(-b) / twoA
				tmp = (float64)(tmp2 - sqrt/twoA)
				fmt.Println(tmp)
				fmt.Println("x2 = ")
				tmp = (float64)(tmp2 + sqrt/twoA)
				fmt.Println(tmp)
			}
		}
	}
}
