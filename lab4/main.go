// main
package main

import "fmt"
import "net/http"
import "strconv"
import "math"

type Message string

var HttpSendMessage Message = ""

//Завдання 1
//func (send Message) ServeHTTP(w http.ResponseWriter, r *http.Request) {
//	msg := "Точка не входить в область"
//	ix := r.FormValue("x")
//	iy := r.FormValue("y")
//	x, _ := strconv.ParseFloat(ix, 32)
//	y, _ := strconv.ParseFloat(iy, 32)
//	if z1(x, y) {
//		msg = "Точка входить в область"
//	}
//	fmt.Fprintf(w, "%v%s", send, msg)
//}

//завдання 2
func (send Message) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ix := r.FormValue("x")
	iy := r.FormValue("y")
	iz := r.FormValue("z")
	x, _ := strconv.ParseFloat(ix, 32)
	y, _ := strconv.ParseFloat(iy, 32)
	z, _ := strconv.ParseFloat(iz, 32)
	fmt.Fprintf(w, "%vРезультат рівняння = %f", send, z2(x, y, z))
}

//Завдання 3
//func (send Message) ServeHTTP(w http.ResponseWriter, r *http.Request) {
//	ix := r.FormValue("x")
//	x, _ := strconv.ParseInt(ix, 10, 32)
//	fmt.Fprintf(w, "%v Місяць %d, %s", send, int64(x), z3(x))
//}

func main() {
	http.ListenAndServe("localhost:2000", HttpSendMessage)
}

func z1(x float64, y float64) bool {
	if x >= 0 && y <= 0 {
		return true
	}
	if x <= y-1 && y >= x+1 {
		return true
	}
	return false
}

func z2(x float64, y float64, z float64) float64 {
	var maxElem = float64(max(x, y, z))
	var minElem = float64(min(x, y, z))
	var u = (math.Pow(maxElem, 2) - math.Pow(2, x)*math.Pow(minElem, 3)) / (math.Cos(5*x) + maxElem/minElem)
	return u
}

func z3(x int64) string {
	str := ""
	switch x {
	case 1:
		fallthrough
	case 2:
		fallthrough
	case 3:
		str = "Перший квартал"
	case 4:
		fallthrough
	case 5:
		fallthrough
	case 6:
		str = "Другий квартал"
	case 7:
		fallthrough
	case 8:
		fallthrough
	case 9:
		str = "Третій квартал"
	case 10:
		fallthrough
	case 11:
		fallthrough
	case 12:
		str = "Четвертий квартал"
	}
	return str
}

func max(x float64, y float64, z float64) float64 {
	if x > y {
		if x > z {
			return x
		}
	}
	if y > z {
		return y
	}

	return z
}

func min(x float64, y float64, z float64) float64 {
	if x < y {
		if x < z {
			return x
		}
	}
	if y < z {
		return y
	}

	return z
}
